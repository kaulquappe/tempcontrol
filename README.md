# **Temperature Control**


A basic temperature control example that reads in a temperature via a K-Type thermocouple sensor.
The volt output of the sensor is processed by an MAX31855 board and read out by an Arduino Nano.
The so gained temperature value is given out to the serial console of the Arduino Nano.
On an output pin of the arduino nano a Solid State Relay (SSR) is connected. 
A PID controller is switching the SSR till the given target temperature is reached.


## Instructions


### What you need:

* [Thermocouple](https://www.banggood.com/de/Universal-K-Type-EGT-Thermocouple-Temperature-Sensors-For-Exhaust-Gas-Probe-p-1011377.html?rmmds=search&cur_warehouse=CN)
* [MAX31855 K-Type thermoelement breakout board for arduino](https://www.banggood.com/de/MAX31855-K-Type-Thermocouple-Breakout-Board-Temperature-Measurement-Module-For-Arduino-p-1086523.html?currency=EUR)
* [Arduino Nano  (clone)](https://www.banggood.com/de/ATmega328P-Arduino-Compatible-Nano-V3-Improved-Version-No-Cable-p-959231.html?gmcCountry=AT&currency=EUR&createTmp=1&utm_source=googleshopping&utm_medium=cpc_elc&utm_content=zouzou&utm_campaign=pla-at-arduino-pc&gclid=EAIaIQobChMIm8_gxOD-1gIVBl8ZCh32wQj4EAQYAiABEgIKwfD_BwE&cur_warehouse=CN)
* [Solid State Relay](https://www.banggood.com/80A-SSR-80DA-Solid-State-Relay-Module-DC-To-AC-24V-380V-Output-p-1097188.html)
* you will also need a soldering iron, jumper cables, breadbord etc.


### Setup

__Hardware__

1. Resolder the plug onto the other side of the MAX31855K breakout board, so that it can be easily attach to the breadboard.

![Original MAX31855K breakout board](docs/img/MAX31855K_Breakout_front.jpg)
![Original MAX31855K breakout board back](docs/img/MAX31855K_Breakout_back.jpg)
![MAX31855K breakout board resoldered](docs/img/MAX31855K_Breakout_resoldered.jpg)

2. The pinout diagram of the Arduino nano is as follows:

![Arduino Nano pinout diagram](docs/img/ArduinoNanoPinoutDiagram.png)

3. Connect the Arduino nano to the MAX31855K breakout board according to following circuit diagram: 

![Wiring diagram](docs/img/tempControlWiring.png)
![Breadboard wiring](docs/img/BreadboardWiring.jpg)
![Setup overview](docs/img/SetupOverview.jpg)

* You can find the Fritzing wiring diagram file (tempControl.fzz) in the docs folder.

__Software__

1. Install the newest Arduino IDE from https://www.arduino.cc/en/main/software (version 1.8.13 at time of writing). 

2. If you need help using Arduino have a look at www.arduino.cc/en/Guide/HomePage. 
    
4. Open "Menu->Sketch->Include Libraries->Manage Libraries.." in the Arduino IDE and install the following libraries:
    * Adafruit_BusIO  (version 1.7.0 at time of writing)
    * Adafruit_MAX31855_library (version 1.2.1 at time of writing)
    * PID by Brett Beauregard (version 1.2.0 at time of writing; only for tempControl_polling.ino)

### Programming

1. Start arduino and in the "Tools" menu set your board to "Arduino Nano".
2. In the "Tools" menu select your processor and the apropriate bootlader ("ATmega 328P (Old Bootloader)" in my case).
3. Open the "tempControl.ino" file that you can find in the src/tempControl directory and "Verify" and "Upload" it to your Arduino board via USB connection   .
4. Once the code upload finishes verifying, open the serial monitor (found in the "Tools" menu) and adjust the baudrate to 115200. 
5. Now you can enter commands found in the file "Commands.h" to your serial monitor and get the response (in debug mode) from the microcontroller like it can be seen in the picture below:
![Serial monitor screenshot](docs/img/SerialMonitorScreenshot.png)
6. SerialMonitor command example:
    * setSetpoint: L45
    * getSetpoint: F (output 45.00)
7. Parameters explained:
    * processValue:   output from sensor (e.g. temperature)
    * controlValue:   input into plant (e.g. heating period interval in ms)
    * setPoint:       target value for measured medium (e.g. target temperature for water)
    * sampleInterval: interval between sensor measurements and maximum input interval to plant
8. Debugging:
    * to enable debugging information uncomment this line in tempControl.ino: //#define _DEBUG1_

### Explanation

__1. K Type Thermocouple__

A thermocouple is a kind of temperature sensor, simply made by welding together two metal wires. Because of a physical effect of two joined metals, there is a slight but measurable voltage across the wires that increases with temperature (Seebeck-Effekt). The main advantage of using a thermocouple is the high temperature range (our K type: -200°C to 1200°C). A difficulty in using them is that the voltage to be measured is very small, with changes of about 50 uV per °C. To solve this we are using a thermocouple interface chip (MAX31855K) to measure the voltage between the wires. If you find that the thermocouple temperature goes down instead of up when heated, when connecting the K type thermocouples with the MAX31855K, try swapping the red and yellow wires.

__2. MAX31855K__

The MAX31855K is a chip that converts the voltage output of the thermocouple to a digital temperature value that is read out via SPI 
protocol. The first assembly step is creating a reliable, electrical connection from the MAX31855K breakout board to your Arduino. We chose 
to use a breakout board because we wanted to connect it to a bread board. The hookup is fairly straightforward. Connect the Arduino, MAX31855K and thermocouple as described in the Fritzing wiring diagram.

__3. Arduino nano__

The Arduino nano communicates with the MAX31855K through SPI protocol. For further details see next point.

__4. SPI protocol__

The SPI is a fast synchronous protocol that tipically uses 4 pins for communication, wiz. MISO, MOSI, SCK, and SS. These pins are directly related to the SPI bus interface.

    1. MISO – MISO stands for Master In Slave Out. MISO is the input pin for Master AVR, and output pin for Slave AVR device. Data transfer from Slave to Master takes place through this channel.
    2. MOSI – MOSI stands for Master Out Slave In. This pin is the output pin for Master and input pin for Slave. Data transfer from Master to Slave takes place through this channel.
    3. SCK – This is the SPI clock line (since SPI is a synchronous communication).
    4. SS – This stands for Slave Select.
    
__5. PID controller__

PID Controller is a most common control algorithm used in industrial automation & applications. PID controllers are used for precise and accurate control of various parameters. Most often these are used for the regulation of temperature, pressure, speed, flow and other process variables. For in depth information about how PID controller work see reference section.

## License

Distributed under the GNU LGPL v.3.0.

## References

* [Thermocouple](https://www.banggood.com/de/Universal-K-Type-EGT-Thermocouple-Temperature-Sensors-For-Exhaust-Gas-Probe-p-1011377.html?rmmds=search&cur_warehouse=CN)
* [MAX31855 K-Type thermoelement breakout board for arduino](https://www.banggood.com/de/MAX31855-K-Type-Thermocouple-Breakout-Board-Temperature-Measurement-Module-For-Arduino-p-1086523.html?currency=EUR)
* [Arduino Nano clone](https://www.banggood.com/de/ATmega328P-Arduino-Compatible-Nano-V3-Improved-Version-No-Cable-p-959231.html?gmcCountry=AT&currency=EUR&createTmp=1&utm_source=googleshopping&utm_medium=cpc_elc&utm_content=zouzou&utm_campaign=pla-at-arduino-pc&gclid=EAIaIQobChMIm8_gxOD-1gIVBl8ZCh32wQj4EAQYAiABEgIKwfD_BwE&cur_warehouse=CN)
* [Solid State Relay](https://www.banggood.com/80A-SSR-80DA-Solid-State-Relay-Module-DC-To-AC-24V-380V-Output-p-1097188.html)
* [henrysbench arduino-max31855 tutorial](http://henrysbench.capnfatz.com/henrys-bench/arduino-temperature-measurements/max31855-arduino-k-thermocouple-sensor-manual-and-tutorial/)
* [MAX31855 specs and datasheet](https://datasheets.maximintegrated.com/en/ds/MAX31855.pdf)
* [ATmega328 Datasheet](http://www.atmel.com/Images/Atmel-42735-8-bit-AVR-Microcontroller-ATmega328-328P_Datasheet.pdf)
* [Arduino Nano Pinout Diagram](http://www.pighixxx.com/test/wp-content/uploads/2014/11/nano.png)
* [Arduino Anleitung](www.arduino.cc/en/Guide/HomePage)
* [K-Type Thermocouple specs](https://www.thermocoupleinfo.com/type-k-thermocouple.htm)
* [PID diagrams](http://ww1.microchip.com/downloads/en/AppNotes/Atmel-2558-Discrete-PID-Controller-on-tinyAVR-and-megaAVR_ApplicationNote_AVR221.pdf)
* [Working of a PID controller](https://www.elprocus.com/the-working-of-a-pid-controller/)
* [PID controller](http://manuals.chudov.com/Servo-Tuning/PID-without-a-PhD.pdf)
* [PID controlling algorithm](http://brettbeauregard.com/blog/2011/04/improving-the-beginners-pid-introduction/)
* [PID Arduino library](https://github.com/br3ttb/Arduino-PID-Library)
* [PID for dummies](https://www.csimn.com/CSI_pages/PIDforDummies.html)

## TODOs
* Add example screenshot that shows how to communicate over serial monitor in the "Programming" section of this README.md!

## Authors

* kaulquappe (https://bitbucket.org/kaulquappe)
* Cubemast3r (https://bitbucket.org/Cubemast3r)
 
