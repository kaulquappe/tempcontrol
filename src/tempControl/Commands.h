
enum CommandId
{
    GETTERSTART         = 64,   // @
    getP                = 65,   // A
    getI                = 66,   // B
    getD                = 67,   // C
    getProcessValue     = 68,   // D
    getControlValue     = 69,   // E
    getSetpoint         = 70,   // F
    getSampleInterval   = 71,   // G
    SETTERSTART         = 72,   // H
    setP                = 73,   // I
    setI                = 74,   // J
    setD                = 75,   // K
    setSetpoint         = 76,   // L
    //setSampleInterval   = 77,   // M
    SETTEREND           = 77    // N
};
